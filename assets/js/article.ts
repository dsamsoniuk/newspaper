
const $ = require('jquery');


$('#add-comment').on('click', function() {
    var data = $('form[name=comment]').serialize();
    var articleId = $(this).data('article-id');
    let box_loader = $(this);
    let loader = $('<img src="/gfx/loader.gif" class="loader">');

    box_loader
        .hide()
        .closest('.loader_box')
        .append(loader);

    $.ajax({
        url: '/comment/add/' + articleId, 
        type: "POST",
        data: data
      })
      .done(function(r) {
        if (r.info === "") {
            $("#comment_list")
                .append(r.html);
            box_loader
                .show()
                .closest('.loader_box')
                .find('.loader')
                .remove();
            $('#commentInfo').addClass('hidden');

        } else {
            $('#commentInfo').html(r.info).removeClass('hidden');
            box_loader
            .show()
            .closest('.loader_box')
            .find('.loader')
            .remove();
        }
    });
});


$('#article-delete').on('click', function(){
    var id = $(this).data('article-id');
    window.location.replace("/article/remove/" + id);
});



var articleTags = {
    $tagsHolder:{},
    init: function(){

        this.$tagsHolder = $('.tags');

        this.$tagsHolder.data('index', this.$tagsHolder.find(':input').length);

        $('.add_tag_link').on('click', function(e) {
            articleTags.addTagForm();
        });
        this.$tagsHolder.on('click', 'li .remove-tag', function(){
            $(this).closest('li').remove();
        });

    },
    addTagForm: function(){
        var prototype = this.$tagsHolder.data('prototype');
        var index = this.$tagsHolder.data('index');
        var newForm = prototype;
        var $removeFormButton = $('<button type="button" class="btn btn-danger remove-tag">usuń</button>');

        newForm = newForm.replace(/__name__/g, index);

        this.$tagsHolder.data('index', index + 1);

        var $newFormLi = $('<li class="list-group-item"></li>').append(newForm).append($removeFormButton);
        this.$tagsHolder.append($newFormLi);
    },

}

$(document).ready(function() {
    articleTags.init();
});
