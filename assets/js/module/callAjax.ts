
const $ = require('jquery');

function callAjax(url, data, callback){

  $.ajax({
    url: url, //wymagane, gdzie się łączymy
    type: "GET",
    data: data
  })
  .done(function(r) {
    callback(r)
  });

};

export { callAjax }
