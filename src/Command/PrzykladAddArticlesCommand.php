<?php

namespace App\Command;

use App\Entity\Article;
use App\Service\ArticleService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PrzykladAddArticlesCommand extends Command
{
    /** @var ArticleService $articleService */
    private $articleService;

    public function __construct(ArticleService $articleService)
    {
        parent::__construct();
        $this->articleService = $articleService;
    }
    protected static $defaultName = 'przyklad:add-articles';

    protected function configure()
    {
        $this
            ->setDescription('Dodaj przykladowe artykuly')
            // ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            // ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $as = $this->articleService;

        $articleList = [
            [
                'title' => 'Sony’s Mark Cerny provides a ‘deep dive’ of the PS5 for developers      ',
                'content' => 'Sony’s chief system architect Mark Cerny has given a “deep dive” of the PS5 console hardware for developers.
                The PS5’s base hardware features a custom eight-core AMD Zen 2 CPU clocked at 3.5GHz along with a custom GPU based on AMD’s RDNA 2 architecture which promises 10.28 teraflops and 36 compute units clocked at 2.23GHz. The processors are accompanied by 16GB of GDDR6 RAM and a custom 825GB SSD.
                For a quick comparison to the recently-unveiled Xbox Series X, Microsoft’s console has the PS5 beat in raw specs on paper. The Xbox Series X features an eight-core processor clocked at 3.8GHz, a GPU with 12 teraflops and 52 compute units each clocked at 1.825GHz. Like Sony’s console, Microsoft’s new hardware comes with 16GB of GDDR6 RAM.',
                'author' => 'Jacek Kowalski',
            ],
            [
                'title' => 'Safari soon won’t accept HTTPS certificates longer than 13 months                ',
                'content' => 'Apple announced during last week’s CA/Browser Forum that Safari will soon reject any HTTPS certificates that expire in any longer than 13 months.
                The CA/Browser Forum is a voluntary consortium that began in 2005 as part of an effort among certification authorities and browser software vendors to provide greater assurance to web users about the sites they visit.
                HTTPS certificates, using TLS encryption, help to ensure the sites a user visits are safe and legitimate. However, just because a site was once safe doesn’t mean that it still is.
                Certificate authorities once used to even issue certificates with ten years of validity. In 2017, the maximum length was reduced to 825 days – which many people believed was still too long and failed to offer sufficient protection to web users.',
                'author' => 'Pawel Kwiatek',
            ],
            [
                'title' => 'Ripple’s dev platform Xpring is ‘building bridges’ with Ethereum                ',
                'content' => 'Ripple and Ethereum may be about to prove that friendliness can go a long way in the cryptosphere with a bridge between the platforms.

                The two platforms really have quite different goals. Ripple is aiming to become #TheStandard when it comes to instant payments and remittances, while Ethereum wants to become the world’s first decentralised computer.
                
                Going by market cap and partnerships, both are the closest in crypto to achieving those goals.
                
                With a current market cap of $28.3 billion, Ethereum is the largest smart contract platform and second-largest cryptocurrency. There are currently 2,775 Dapps on Ethereum and the Enterprise Ethereum Alliance is partnered with over 180 large global companies including AMD, Baidu, Intel, Microsoft, and others.
                
                Ripple is the third-largest cryptocurrency with a current market cap of $11.9 billion. The company’s flagship solution, RippleNet, is available in 40 countries across six continents and makes it easy to connect and transact across a network of 200+ financial institutions.
                
                Together, Ripple and Ethereum could be a force to be reckoned with given their individual strengths.',
                'author' => 'Witold Woda',
            ],
        ];
        
        foreach ($articleList as $art) {
            $article = new Article();
            $article->setAuthor($art['author']);
            $article->setContent($art['content']);
            $article->setTitle($art['title']);
            $as->create($article);
            $output->writeln(["Dodany artykul ". $art['title']]);
        }

        return 0;
    }
}
