<?php


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use App\Entity\Category;
use App\Service\CategoryTree;

class RefreshArticlesCommand extends ContainerAwareCommand 
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'refresh-articles';

    public function __construct(CategoryTree $tree) {
        $this->tree = $tree;
        parent::__construct();
    }

    protected function configure()
    {
        // $this
        //     // the short description shown while running "php bin/console list"
        //     ->setDescription('Creates a new user.')
        //     ->setHelp('This command allows you to create a user...')
        // ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $output->writeln([
            "Refresh last 5 articles from each category",
            "==========================================="
        ]);

        $articles_by_category = [];
        $main_categories = $doctrine->getRepository(Category::class)->findBy(['parent' => NULL]);
        $output->writeln([
            "Found ".count($main_categories)." categories",
            "==========================================="
        ]);
        foreach ($main_categories as $category) {
            $articles = $this->tree->getArticlesByCategoryName( $category->getName() );
            $articles_by_category[ $category->getName() ] = $articles;
        }

        $cache = new FilesystemCache();
        $cache->set('articles', $articles_by_category);

    }
}