<?php

// src/Controller/LuckyController.php
namespace App\Controller\Article;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Entity\ArticleComment;
use App\Repository\ArticleCommentRepository;
use App\Repository\ArticleRepository;
use App\Entity\Comment;
use App\Entity\Tag;
use App\Form\AddArticleType;
use App\Form\CommentType;
use App\Form\EditArticleType;
use App\Service\ArticleService;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;

class ArticleController extends AbstractController
{
    /** @var ArticleCommentRepository $articleCommentRepo */
    private $articleCommentRepo;

    /** @var ArticleRepository $articleRepo */
    private $articleRepo;

    private $articleService;

    public function __construct(
        ArticleCommentRepository $articleCommentRepo, 
        ArticleRepository $articleRepo,
        ArticleService $articleService
    ){
        // parent::__construct();
        $this->articleCommentRepo = $articleCommentRepo;
        $this->articleRepo = $articleRepo;
        $this->articleService = $articleService;
    }
    /**
     * @Route("/article/{article}", name="article_details", requirements={"article":"\d+"})
     * @var Request $request
     * @var Article $article
     */
    public function details(Request $request, Article $article = null)
    {
        $comments = [];
        if ($article) {
            $comments   = $this->articleCommentRepo->findComments($article->getId());
        }
        $com    = new Comment();
        $form   = $this->createForm(CommentType::class, $com);

        return $this->render('article/article.html.twig', [
            "article"   => $article,
            "comments"  => $comments,
            'form'      => $form->createView(),
            // "article"   => $article
        ]);
    }

    /**
     * @Route("/comment/add/{article}", name="add_comment", requirements={"article":"\d+"})
     * @var Request $request
     * @var Article article
     */
    public function addComment(Request $request, Article $article)
    {
        $em         = $this->getDoctrine()->getManager();
        $html       = '';
        $info       = '';
        $comment    = new Comment();
        $form       = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        try {
            if (!$form->isValid()) {
                throw new Exception("Nie poprawnie wypełniony formularz");
            }

            $ac = new ArticleComment();
            $ac->setArticle($article);
            $ac->setComment($comment);
            $ac->setDateAdd(new \DateTime());
            $em->persist($ac);
            $em->flush();

            $html = $this->render('default/comment_row.html.twig', ['comment' => $comment])->getContent();

        } catch( Exception $e) {
            $info = $e->getMessage();
        }
    
        return new JsonResponse([
            'info' => $info,
            'html' => $html
        ]);
    }
    /**
     * @Route("/article/add", name="add_article", requirements={
     *     "_locale"="en|pl"
     * })
     * @var Request $request
     * @IsGranted("ROLE_ADMIN")
     */
    public function add(Request $request)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $article = new Article();

        $form = $this->createForm(AddArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($user->getUsername());
            $bgImage = $form['background_image']->getData();

            if ($bgImage) {
                $newFilename = uniqid().'.'.$bgImage->guessExtension();

                try {
                    $bgImage->move($this->getParameter('article_image_directory'), $newFilename);
                } catch (FileException $e) {
                    $this->addFlash('danger', $e->getMessage());
                }
                $article->setBackgroundImage($newFilename);
            }
          
            // $article->setCategories($articleCategory);
            $this->articleService->create($article);

            $this->addFlash('notice','Artykuł dodany');
        }

        return $this->render('article/article.add.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/article/remove/{article}", name="remove_article", requirements={"article":"\d+"})
     * @var Article $article
     * @IsGranted("ROLE_ADMIN")
     */
    public function remove(Article $article){
        if ($article) {
            $em   = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
            $this->addFlash(
                'danger',
                'Artykuł usunięty'
            );
        }

        return $this->redirect( $this->generateUrl('main_index', array() ) );
    }
    /**
     * @Route("/article/edit/{article}", name="edit_article", requirements={"article":"\d+"})
     * @var Request $request
     * @var Article $article
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Article $article){
        $em = $this->getDoctrine()->getManager();
        if (!$article) {
            $article = new Article();
        }
        $this->addFlash(
            'danger',
            'Artykuł zmodyfikowany'
        );
        $form = $this->createForm(EditArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

       
        }

        return $this->render('article/article.edit.html.twig', [
            'form'      => $form->createView(),
        ]);

    }
}