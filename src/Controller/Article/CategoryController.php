<?php

// src/Controller/LuckyController.php
namespace App\Controller\Article;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Simple\FilesystemCache;

use App\Entity\Category;

class CategoryController extends AbstractController
{

    /**
     * @Route("/{_locale}/category/{name}", name="article_categories")
     */
    public function index($name)
    {

        $choosen_articles = [];

        $cache = new FilesystemCache();
        $categories = [];

        if ( $cache->has('articles')) {
            $categories = $cache->get('articles');
            if (isset($categories[ $name ])) {
                $choosen_articles = $categories[ $name ];
            }
        }

// dump($choosen_articles);
// die;


        return $this->render('category/category.html.twig', [
            "articles" => $choosen_articles,
            "category_name" => $name
        ]);
    }


}