<?php

namespace App\Controller;

use App\Entity\ArticleCategory;
use App\Entity\Category;
use App\Form\CategoryTreeType;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\ArticleService;

class CategoryController extends AbstractController
{
    public function __construct(ArticleRepository $articleRepository, ArticleService $articleService)
    {
        $this->articleRepository = $articleRepository;
        $this->articleService = $articleService;
    }
        /**
     * @Route("/category/add", name="category.add")
     * @var Article $article
     */
    // public function add(Request $request)
    // {
    //     $form = $this->createForm(CategoryType::class);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->articleService->add($form->getData());
    //     }

    //     return $this->render('category/add.html.twig', [
    //         'form' => $form->createView()
    //     ]);
    // }
    /**
     * @Route("/category/{article}", name="category")
     * @var Article $article
     */
    // public function index(Request $request, Article $article)
    // {

    
    //     return $this->render('category/index.html.twig', [
    //         // 'form' => $form->createView()
    //     ]);
    // }
}
