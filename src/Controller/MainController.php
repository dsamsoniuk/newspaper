<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Service\CategoryTree;

class MainController extends AbstractController
{
    /** @var ArticleRepository $articleRepo */
    private  $articleRepo;

    public function __construct(
        CategoryTree $category_tree,
        ArticleRepository $articleRepo
    ) {
        $this->category_tree    = $category_tree;
        $this->articleRepo      = $articleRepo;
    }

    // /**
    //  * @Route("/", name="base_index")
    //  */
    //  public function base(Request $request) {

    //     return $this->redirect( $this->generateUrl('main_index', array(
    //         "_locale" => $request->getLocale()
    //     ) ) );
    //  }

    /**
     * @Route("/", name="main_index")
     */
    public function index(Request $request)
    {

        return $this->render('main.html.twig', [
            // 'categories' => $categories,
            'articles' => $this->articleRepo->findBy([], ['id' => 'desc'])
        ]);
    }


}