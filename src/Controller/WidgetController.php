<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class WidgetController extends AbstractController
{

    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }
    /**
     * @Route("/widget", name="widget")
     */
    public function index()
    {
        $article = $this->articleRepository->findNeawest();

        return $this->render('widget/index.html.twig', [
            'article' => $article
        ]);
    }
}
