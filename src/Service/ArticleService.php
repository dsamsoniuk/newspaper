<?php

namespace App\Service;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Category;

class ArticleService
{
    /** @var EntityManagerInterface entityManager */
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }
    public function create(Article $article = null)
    {
        if (!$article) {
            $article = new Article();
        }
        $em = $this->entityManager;
        $article->setDatePublish(new DateTime());
        $em->persist($article);
        $em->flush();


        return $article;
    }

    public function add(Category $category){
        $em = $this->entityManager;
        $em->persist($category);
        $em->flush();
    }
    public function updateCategories(Article $ac){
        $em = $this->entityManager;
        $em->persist($ac);
        $em->flush();
    }
}