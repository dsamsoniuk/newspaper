<?php

namespace App\Service;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Category;
use App\Entity\ArticleCategory;

class CategoryTree
{

    public function __construct(EntityManagerInterface $entityManager) {
        $this->em = $entityManager;
    }

    public function generate()
    {

        $cat = $this->em
        ->getRepository(Category::class)->findAll();

        // dump($cat);
        $tree = [];

        for ($i=0; $i<count($cat); $i++) {
            if (!$cat[$i]->getParent()) {
                $tree[] = [
                    'id'    => $cat[$i]->getId(),
                    'name'  => $cat[$i]->getName(),
                ];
            }
        }

        for ($i=0, $count=count($cat); $i < $count; $i++) {

            $current_category = $cat[$i];
            foreach ($tree as &$t) {

                if (!$current_category->getParent()) {
                    continue;
                }
                $t = $this->addToTreeCategory($t, $current_category->getParent()->getId(), [
                    'id'    => $current_category->getId(),
                    'name'  => $current_category->getName(),
                ]);
            }
        }        

        return $tree;
    }

    private function addToTreeCategory($branch, $parent_id, $new_child){

        if (isset($branch['id']) && $branch['id'] == $parent_id) {
            if (!isset($branch['children'])) {
                $branch['children'] = [];
            }
            // $new_child['status'] = 'added';
            $branch['children'][] = $new_child;
        }
        else if (isset($branch['children'])) {

            foreach ($branch['children'] as &$child) {
                $child = $this->addToTreeCategory($child, $parent_id, $new_child);
            }
        }
        return $branch;
    }

    public function getSubcategoryList($category) {

        $tree               = $this->generate();
        $subcategory_list   = [];

        if(!$category) {
            return $subcategory_list;
        }
        foreach ($tree as $branch) {
            $collection = $this->getRecursiveCategories($branch, [], $category->getId(), false);
            if (!empty($collection)) {
                $subcategory_list = $collection;
                break;
            }
        }
        return $subcategory_list;
    }
    /**
     * Szukaj w drzewie categorii podkategorie wybranej kategorii
     */
    private function getRecursiveCategories($branch, $list, $search_cat_name, $canSave) {

        if ($branch['id'] == $search_cat_name) {
            $canSave = true;
        }

        if ($canSave && !isset($branch['children'])) {
            return $list;
        }

        if (isset($branch['children'])) {
            foreach ($branch['children'] as $child) {
                if ($canSave) {
                    $list[] = $child['id'];
                }
                $new_list = $this->getRecursiveCategories($child, $list,  $search_cat_name, $canSave);
                $list = array_unique (array_merge ($new_list, $list));
            }
        } 
        
        return $list;
    }

    /**
     * Pobierz wszystkie artykuły wg. kategory
     */
    public function getArticlesByCategoryName($name ) {

        $category = $this->em
            ->getRepository(Category::class)
            ->findOneBy(["name" => $name]);

        $tree = $this->getSubcategoryList($category);

        $articles = $this->em
            ->getRepository(ArticleCategory::class)
            ->findArticlesThroughSubcategories( $tree );

        return $articles;
    }

}