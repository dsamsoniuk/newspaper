<?php

// src/Twig/AppExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Component\HttpFoundation\RequestStack;

class AppExtension extends AbstractExtension
{
    public function __construct( RequestStack  $request) {
        $this->request = $request;

    }
    public function getFilters()
    {
        return array(
            new TwigFilter('url', array($this, 'formaUrl')),
        );
    }

    public function formaUrl($url)
    {
        $locale = $this->request->getCurrentRequest()->getLocale();

        return "/".$locale.$url;
    }


    // public function getFunctions()
    // {
    //     return array(
    //         new TwigFunction('area', array($this, 'calculateArea')),
    //     );
    // }
    // public function calculateArea(int $width)
    // {
    //     return $width * 2;
    // }


}